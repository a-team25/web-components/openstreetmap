# Progressbar - Web Komponente

## Installation

### Npm
```bash
echo @a-team25:registry=https://gitlab.com/api/v4/projects/47253819/packages/npm/ >> .npmrc
```

```bash
npm i @a-team25/at-open-street-map
```

### Yarn
```bash
echo \"@a-team25:registry\" \"https://gitlab.com/api/v4/projects/47253819/packages/npm/\" >> .yarnrc
```

```bash
yarn add @a-team25/at-open-street-map
```

## Benutzung

### Import
The component only needs to be imported in the "main" JavaScript file of the project.<br>
After the import the component **global** is available in the project and nothing else has to be done.

```javascript
import '@a-team25/at-open-street-map'
```

### Attribute

Work in Progress ...

### Template

```html
<at-open-street-map
    longitude="7.670624"
    latitude="50.426523"
    zoom="14"
    zoom-extension
    fullscreen-extension
    scale-line-extension
    rotate-extension
    zoom-slider-extension
>
    <at-open-street-map-layer
        src="https://upload.wikimedia.org/wikipedia/commons/e/ed/Map_pin_icon.svg"
        icon-type="marker"
        cluster-offset-Y="-30"
        cluster-min-distance="100"
        cluster-distance="200"
    >
        <at-open-street-map-poi longitude="7.670624" latitude="50.426523">
            <div class="example-marker">
                <p class="title"><b>Example</b></p>
                <div class="content">
                    <p class="address">
                        Examplestreet 27
                        12345 Example-City
                    </p>

                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad alias, animi architecto beatae esse est eveniet excepturi fugit itaque molestias nihil nisi officia omnis placeat provident quisquam similique sit temporibus, veniam vero. Debitis illo itaque labore maxime minus molestias nostrum obcaecati perferendis porro, quidem, quis quod, similique sunt voluptatem voluptatum.
                    </p>
                </div>
            </div>
        </at-open-street-map-poi>
    </at-open-street-map-layer>
</at-open-street-map>
```
