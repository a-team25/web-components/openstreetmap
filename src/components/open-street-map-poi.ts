import {LitElement} from 'lit';
import {customElement, property} from 'lit/decorators.js';

@customElement('adt-open-street-map-poi')
export class AdtOpenStreetMapPoi extends LitElement {

    @property()
    longitude?: number = 0;

    @property()
    latitude?: number = 0;
}
