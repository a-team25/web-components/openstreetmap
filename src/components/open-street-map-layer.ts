import {LitElement} from 'lit';
import {customElement, property} from 'lit/decorators.js';

@customElement('adt-open-street-map-layer')
export class AdtOpenStreetMapLayer extends LitElement {

    @property()
    label?: string = '';

    @property()
    src?: string = '';

    @property()
    iconType?: string = undefined;

    @property()
    markerHeight?: number = 50;

    @property()
    cluster?: boolean = false;

    @property()
    clusterMinDistance?: number = 100;

    @property()
    clusterDistance?: number = 100;

    @property()
    clusterOffsetX?: number = 0;

    @property()
    clusterOffsetY?: number = -30;

    @property()
    showClusterText?: boolean = false;
}
