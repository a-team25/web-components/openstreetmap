import {Control} from "ol/control";
import {fromLonLat} from "ol/proj";
import SourceVector from "ol/source/Vector";
import Feature from "ol/Feature";
import {Point} from "ol/geom";
import {Text} from "ol/style";
import Fill from "ol/style/Fill";
import Style from "ol/style/Style";
import Circle from "ol/style/Circle";
import VectorLayer from "ol/layer/Vector";
import Map from "ol/Map";

export type Translations = {
    currentLocation?: string;
}

export type Options = {
    target?: string | HTMLElement | undefined;
    translations?: Translations | undefined;
    gpsLocationIcon?: HTMLElement | null
};

export class GpsLocation extends Control {

    _options?: Options;

    constructor(opt_options: Options) {
        const options: Options = opt_options || {};

        const button = document.createElement('button');
        button.innerHTML = options?.gpsLocationIcon?.outerHTML ?? 'X';

        const element = document.createElement('div');
        element.className = 'ol-user-location ol-unselectable ol-control';
        element.appendChild(button);

        super({
            element: element,
            target: options.target,
        });

        this._options = options;

        button.addEventListener('click', this.handleGpsLocations.bind(this), false);
    }

    handleGpsLocations() {
        navigator.geolocation.getCurrentPosition(this.onSuccess.bind(this))
    }

    onSuccess(position: GeolocationPosition) {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;

        const map: Map | null = this.getMap();
        const view = map?.getView();

        view?.setCenter(fromLonLat([longitude, latitude]));
        view?.setZoom(17);

        map?.addLayer(this.getVectorLayer(longitude, latitude));
    }

    getVectorLayer(longitude = 0, latitude = 0) {
        const source = new SourceVector({
            features: [
                new Feature({
                    geometry: new Point(fromLonLat([longitude, latitude])),
                })
            ]
        });

        const text = new Text({
            text: this._options?.translations?.currentLocation ?? '',
            offsetX: 0,
            offsetY: -20,
            scale: 1.5,
            fill: new Fill({
                color: '#B71C1C'
            })
        });

        const style: Style = new Style({
            text,
            image: new Circle({
                radius: 10,
                fill: new Fill({
                    color: '#B71C1C'
                })
            })
        });

        return new VectorLayer({
            properties: {
                title: 'Current Location',
            },
            style,
            source
        })
    }
}
