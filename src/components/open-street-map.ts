// Lit
import {LitElement} from 'lit';
import {customElement, property, state} from 'lit/decorators.js';

// Open Layer
import View from 'ol/View';
import Map from "ol/Map";
import Overlay from 'ol/Overlay';
import { fromLonLat } from 'ol/proj';
import TileLayer from "ol/layer/Tile";
import OSM from "ol/source/OSM";
import Feature, {FeatureLike} from "ol/Feature";
import {AdtOpenStreetMapPoi} from "./open-street-map-poi";
import {Geometry, Point} from "ol/geom";
import SourceVector from "ol/source/Vector";
import {Cluster} from "ol/source";
import VectorLayer from "ol/layer/Vector";
import {AdtOpenStreetMapLayer} from "./open-street-map-layer";
import Style, {StyleLike} from "ol/style/Style";
import Circle from "ol/style/Circle";
import Fill from "ol/style/Fill";
import {Icon, Text} from "ol/style";
import {Collection} from "ol";
import {Control} from "ol/control";
import {FullScreen, ScaleLine, Rotate, ZoomSlider, ZoomToExtent, Zoom, Attribution} from 'ol/control';
import {Options as ZoomToExtentOptions} from 'ol/control/ZoomToExtent';
import {Extent} from "ol/extent";
import {GpsLocation, Options as GpsLocationOptions} from "./controls/gps-location";
import {Coordinate} from "ol/coordinate";

interface Layer {
    features: Feature[];
    label: string,
    src?: string
    iconType?: string,
    markerHeight: number,
    showClusterText: boolean,
    cluster: boolean,
    clusterDistance: number,
    clusterMinDistance: number,
    clusterOffsetX: number,
    clusterOffsetY: number
}

interface StyleCache {
    [key: number]: StyleLike;
}

interface VectorCache {
    [key: number]: StyleCache;
}

@customElement('adt-open-street-map')
export class AdtOpenStreetMap extends LitElement {

    static properties = {
        cluster: {
            type: Boolean,
            reflect: true
        }
    };

    @property()
    zoom?: number = 5;

    @property()
    minZoom?: number = 2;

    @property()
    maxZoom?: number = 20;

    @property()
    longitude?: number = 0;

    @property()
    latitude?: number = 0;

    @property({attribute: true, type: Boolean})
    'zoom-extension' = false;

    @property({attribute: true, type: Boolean})
    'fullscreen-extension' = false;

    @property({attribute: true, type: Boolean})
    'scale-line-extension' = false;

    @property({attribute: true, type: Boolean})
    'rotate-extension' = false;

    @property({attribute: true, type: Boolean})
    'zoom-slider-extension' = false;

    @property({attribute: true, type: Boolean})
    'zoom-to-extend-extension' = false;

    @property({attribute: true, type: Boolean})
    'attribution-extension' = false;

    @property({attribute: true, type: Boolean})
    'gps-location-extension' = false;

    @state()
    private _clusterMinDistance = 100;

    @state()
    private _clusterDistance = 100;

    @state()
    private _clusterOffsetX = 0;

    @state()
    private _clusterOffsetY = -29;

    @state()
    private _projection = 'EPSG:4326';

    @state()
    private _map?: Map;

    @state()
    private _styleCache: StyleCache = {};

    @state()
    private _vectorCache: VectorCache = {};

    @state()
    private _markerHeight = 50;

    @state()
    private _fontWeight = 'bolder';

    @state()
    private _fontSize = '16px';

    @state()
    private _fontFamily = 'sans-serif';

    @state()
    private _extent?: Extent;

    @state()
    private _gpsLocationIcon?: HTMLElement|null;

    @state()
    private _markerOverlay?: Overlay|null;

    @state()
    private _markerPopover?: HTMLElement;

    connectedCallback() {
        this.classList.add('at-open-street-map');

        this._map = this._getMap();
        this._map.setView(this._getView());
        this._map.addLayer(this._getTileLayer());

        const layers = this._getLayers();

        this._gpsLocationIcon = this.querySelector('[slot=gps-location-icon]');

        if (this._gpsLocationIcon !== null) {
            this.removeChild(this._gpsLocationIcon)
        }

        layers.forEach((layer: Layer, layerId) => {
            this._vectorCache[layerId] = {};
            this._map?.addLayer(this._getVectorLayer(layer, layerId));
        })

        this._markerOverlay = this._getMarkerOverlay()
        this._map.addOverlay(this._markerOverlay);

        this._loadControls();
        this._registerEvents();
    }

    _getMarkerOverlay() {
        const overlay: HTMLDivElement = document.createElement('div');
        overlay.classList.add('at-open-street-map-popup-overlay');

        this.appendChild(overlay);

        return new Overlay({
            element: overlay,
            stopEvent: false,
        });
    }

    _onMapMove() {
        const overlay = this._markerOverlay;

        this._markerPopover?.['hide']();
        overlay?.setPosition(undefined);
    }

    _registerEvents() {
        this._map?.addEventListener('movestart', this._onMapMove.bind(this))

        this._map?.on('click', (e) => {
            const map: Map = e.map;

            if (map.hasFeatureAtPixel(e.pixel) ?? false) {
                map.forEachFeatureAtPixel(e.pixel, (feature: FeatureLike) => {
                    const features: Feature[] = feature.get('features');

                    if (features.length === 1) {
                        feature = features[0];
                        const content: string = feature.get('content');

                        if (content.trim().length > 0) {
                            const overlay = this._markerOverlay;

                            if (overlay) {
                                const element = overlay.getElement();

                                const point: Point = <Point>feature.getGeometry();
                                const coordinates: Coordinate = point.getCoordinates();

                                overlay.setPosition(coordinates);

                                if (element && window['bootstrap'] !== undefined) {
                                    this._markerPopover  = new window['bootstrap'].Popover(element, {
                                        animation: true,
                                        container: this,
                                        content: content,
                                        html: true,
                                        placement: 'top',
                                    });

                                    this._markerPopover?.['show']();
                                } else if (element && window['bootstrap'] === undefined) {
                                    element.innerHTML = content;
                                }
                            }
                        }
                    }
                })
            } else {
                const overlay = this._markerOverlay;

                this._markerPopover?.['hide']();
                overlay?.setPosition(undefined);
            }
        })
    }

    _removeUnusedControls() {
        if (this._map) {
            const controls: Collection<Control> = this._map.getControls();

            controls.forEach((control: Control) => {
                if (control instanceof Zoom && !this["zoom-extension"]) {
                    this._map?.removeControl(control)
                }
                if (control instanceof FullScreen && !this["fullscreen-extension"]) {
                    this._map?.removeControl(control)
                }
                if (control instanceof ScaleLine && !this["scale-line-extension"]) {
                    this._map?.removeControl(control)
                }
                if (control instanceof Rotate && !this["rotate-extension"]) {
                    this._map?.removeControl(control)
                }
                if (control instanceof ZoomSlider && !this["zoom-slider-extension"]) {
                    this._map?.removeControl(control)
                }
                if (control instanceof ZoomToExtent && !this["zoom-to-extend-extension"]) {
                    this._map?.removeControl(control)
                }
                if (control instanceof Attribution && !this["attribution-extension"]) {
                    this._map?.removeControl(control)
                }
                if (control instanceof GpsLocation && !this["gps-location-extension"]) {
                    this._map?.removeControl(control)
                }
            })
        }
    }

    _controlIsAssigned<T>(control: T) {
        if (this._map) {
            return this._map.getControls().getArray().find((c) => {
              return c.constructor.name === (control as Control)['name'];
            }) !== undefined;
        }

        throw new Error('map is not available')
    }

    _addUsedControls() {
        if (this._map) {
            if (!this._controlIsAssigned(Zoom) && this["zoom-extension"]) {
                this._map.addControl(new Zoom());
            }
            if (!this._controlIsAssigned(FullScreen) && this["fullscreen-extension"]) {
                this._map.addControl(new FullScreen());
            }
            if (!this._controlIsAssigned(ScaleLine) && this["scale-line-extension"]) {
                this._map.addControl(new ScaleLine());
            }
            if (!this._controlIsAssigned(Rotate) && this["rotate-extension"]) {
                this._map.addControl(new Rotate());
            }
            if (!this._controlIsAssigned(ZoomSlider) && this["zoom-slider-extension"]) {
                this._map.addControl(new ZoomSlider());
            }
            if (!this._controlIsAssigned(ZoomToExtent) && this["zoom-to-extend-extension"]) {
                const options: ZoomToExtentOptions = {extent: this._extent}
                this._map.addControl(new ZoomToExtent(options));
            }
            if (!this._controlIsAssigned(Attribution) && this["attribution-extension"]) {
                this._map.addControl(new Attribution());
            }
            if (!this._controlIsAssigned(GpsLocation) && this["gps-location-extension"]) {
                const options: GpsLocationOptions = {
                    gpsLocationIcon: this._gpsLocationIcon,
                    translations: {
                        currentLocation: 'Aktueller Standort',
                    }
                };

                this._map.addControl(new GpsLocation(options));
            }
        }
    }

    _loadControls() {
        this._removeUnusedControls();
        this._addUsedControls();
    }

    _getSourceVector(layer: Layer) {
        const defaultSource = new SourceVector({
            features: layer.features
        })

        this._extent = defaultSource.getExtent();

        return layer.cluster
            ? this._getClusterSource(defaultSource, layer)
            : defaultSource;
    }

    _getClusterSource(source: SourceVector, layer: Layer): Cluster {
        return new Cluster({
            distance: layer.clusterDistance,
            minDistance: layer.clusterMinDistance,
            source
        });
    }


    _getVectorLayer(layer: Layer, layerId: number): VectorLayer<SourceVector<Geometry>> {
        const source = this._getSourceVector(layer);

        return new VectorLayer({
            properties: {
                label: layer.label
            },
            style: function (feature: FeatureLike) {
                const featureCount = feature.get('features')?.length ?? 1;
                let cachedStyle: Style = this._vectorCache[layerId][featureCount];

                if (!cachedStyle) {
                    const vectorLayerStyle: Style = this._getStyle(layer, featureCount);

                    if (layer.cluster && layer.showClusterText && featureCount > 1) {
                        const clusterText: Text = this._getClusterText(featureCount, layer)
                        vectorLayerStyle.setText(clusterText);
                    }

                    cachedStyle = vectorLayerStyle;
                    this._vectorCache[layerId][featureCount] = cachedStyle
                }

                return cachedStyle;

            }.bind(this),
            source
        })
    }

    _getClusterText(featureCount: number, layer: Layer): Text {
        const scaleFactor = this._getScaleFactor(featureCount);

        return new Text({
            font: `${this._fontWeight} ${this._fontSize} ${this._fontFamily}`,
            text: featureCount.toString(),
            offsetX: layer.clusterOffsetX,
            offsetY: layer.clusterOffsetY * scaleFactor,
            fill: new Fill({
                color: '#666'
            })
        })
    }

    _getStyle(layer: Layer, featureCount: number): Style {
        const style: Style = new Style();

        if (layer.iconType === 'marker' && layer.src !== undefined) {
            style.setImage(this._getMarkerStyleIcon(layer, featureCount))
        } else if (layer.iconType === undefined) {
            style.setImage(this._getDefaultStyleIcon());
        }

        return style;
    }

    _getScaleFactor(featureCount: number) {
        let multiplikator = 1;

        switch (true) {
            case (featureCount >= 10 && featureCount < 100): multiplikator = 1.5; break;
            case (featureCount >= 100 && featureCount < 1000): multiplikator = 2; break;
            case (featureCount >= 1000 && featureCount < 10000): multiplikator = 2.5; break;
        }

        return multiplikator;
    }

    _getMarkerStyleIcon(layer: Layer, featureCount: number): Icon {
        return new Icon({
            rotateWithView: false,
            src: layer.src,
            anchor: [0.5, 1],
            anchorXUnits: 'fraction',
            anchorYUnits: 'fraction',
            height: layer.cluster && layer.showClusterText
                ? this._getScaleFactor(featureCount) * layer.markerHeight
                : layer.markerHeight
        })
    }

    _getDefaultStyleIcon(): Circle {
        return new Circle({
            radius: 10,
            fill: new Fill({
                color: '#3399CC'
            })
        })
    }

    _getFeatures(layer: AdtOpenStreetMapLayer): Feature[] {
        const elements: NodeListOf<AdtOpenStreetMapPoi> = layer.querySelectorAll('adt-open-street-map-poi');

        const features: Feature[] = [];

        elements.forEach((element: AdtOpenStreetMapPoi) => {
            let longitude: Attr | null | number = element.attributes.getNamedItem('longitude');
            let latitude: Attr | null | number = element.attributes.getNamedItem('latitude');
            const content: string = element.innerHTML;

            longitude = parseFloat(longitude?.value ?? '0');
            latitude = parseFloat(latitude?.value ?? '0');

            if (!isNaN(longitude) && !isNaN(latitude)) {
                const feature: Feature<Point> = new Feature({
                    geometry: new Point(fromLonLat([longitude, latitude])),
                    content: content
                })

                features.push(feature);
            }

            layer.removeChild(element);
        })

        return features;
    }

    _getLayers(): Layer[] {
        const elements: NodeListOf<AdtOpenStreetMapLayer> = this.querySelectorAll('adt-open-street-map-layer');

        const layers: Layer[] = [];

        elements.forEach((element: AdtOpenStreetMapLayer) => {
            const features = this._getFeatures(element);
            const label = element.attributes.getNamedItem('label')?.value ?? '';
            const src = element.attributes.getNamedItem('src')?.value ?? undefined;
            const iconType = element.attributes.getNamedItem('icon-type')?.value ?? undefined;
            const showClusterText = element.attributes.getNamedItem('show-cluster-text') !== null;
            const cluster = element.attributes.getNamedItem('cluster') !== null ||
                element.attributes.getNamedItem('cluster-min-distance') !== null ||
                element.attributes.getNamedItem('cluster-distance') !== null;


            let clusterOffsetX = parseFloat(element.attributes.getNamedItem('cluster-offset-x')?.value ?? '?');
            let clusterOffsetY = parseFloat(element.attributes.getNamedItem('cluster-offset-y')?.value ?? '?');
            let clusterDistance = parseFloat(element.attributes.getNamedItem('cluster-distance')?.value ?? '?');
            let clusterMinDistance = parseFloat(element.attributes.getNamedItem('cluster-min-distance')?.value ?? '?');
            let markerHeight = parseFloat(element.attributes.getNamedItem('marker-height')?.value ?? '?');

            clusterOffsetX = isNaN(clusterOffsetX) ? this._clusterOffsetX : clusterOffsetX;
            clusterOffsetY = isNaN(clusterOffsetY) ? this._clusterOffsetY : clusterOffsetY;
            clusterDistance = isNaN(clusterDistance) ? this._clusterDistance : clusterDistance;
            clusterMinDistance = isNaN(clusterMinDistance) ? this._clusterMinDistance : clusterMinDistance;
            markerHeight = isNaN(markerHeight) ? this._markerHeight : markerHeight;

            const layer: Layer = {
                features,
                label,
                iconType,
                src,
                markerHeight,
                showClusterText,
                cluster,
                clusterDistance,
                clusterMinDistance,
                clusterOffsetX,
                clusterOffsetY
            }

            layers.push(layer);
            this.removeChild(element);
        })

        return layers;
    }

    _getTileLayer(): TileLayer<OSM> {
        return new TileLayer({
            visible: true,
            source: new OSM()
        })
    }

    _getView(): View {
        return new View({
            center: fromLonLat([Number(this.longitude ?? 0), Number(this.latitude ?? 0)]),
            zoom: this.zoom,
            minZoom: this.minZoom,
            maxZoom: this.maxZoom,
        });
    }

    _getMap(): Map {
        return new Map({
            target: this
        })
    }
}
